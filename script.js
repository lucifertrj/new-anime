function komi() {
    onclick = document.getElementById("anime-name").innerHTML = "Komi-san Can't Communicate";
    onclick = document.getElementById("anime-info").innerHTML = "Komi-san is the beautiful and admirable girl that no one can take their eyes off of. Almost the whole school sees her as the cold beauty that's out of their league, but Tadano Hitohito knows the truth: she's just really bad at communicating with others.Komi - san, who wishes to fix this bad habit of hers, tries to improve herself with the help of Tadano - kun."
    onclick = document.getElementById("show").style.display = "inline";
    onclick = document.getElementById("genre").innerHTML = "Comedy \n Romance \n Drama";
}

function blueperiod() {
    onclick = document.getElementById("anime-name").innerHTML = "Blue Period";
    onclick = document.getElementById("anime-info").innerHTML = "Yatora Yaguchi is a second - year high school student with excellent grades and many friends, an ideal pupil who plays his part almost to perfection.Everyone around him thinks ' wow, he must be so satisfied with life!', but Yatora has always felt empty, like something's missing.But one day he wanders into the art room, and is captivated by a piece he sees on display there."
    onclick = document.getElementById("show").style.display = "inline";
    onclick = document.getElementById("genre").innerHTML = "Slice Of Life \n Drama";
}

function platinumend() {
    onclick = document.getElementById("anime-name").innerHTML = "Platinum End";
    onclick = document.getElementById("anime-info").innerHTML = "Mirai Kakehashi, a young man who decides to kill himself following his family's passing and suffering from extended abuse from his relatives and peers, is saved by the angel Nasse. The otherworldly-being informs him that God is retiring and that she chose him as a God Candidate. He now has to compete in a competition with other God Candidates where whoever promises themselves the most worthy or is the last to survive the trial will become the next God.";
    onclick = document.getElementById("show").style.display = "inline";
    onclick = document.getElementById("genre").innerHTML = "Action \n Psychological \n Mystery";
}

function mierukachan() {
    onclick = document.getElementById("anime-name").innerHTML = "Mieruka Chan";
    onclick = document.getElementById("anime-info").innerHTML = "All of a sudden, Miko is able to see grotesque monsters all around her; but no one else can. Rather than trying to run away or face them, she instead musters all of her courage and...ignores them. Join in on her day-to-day life as she keeps up her best poker face despite the supernatural goings-on.";
    onclick = document.getElementById("show").style.display = "inline";
    onclick = document.getElementById("genre").innerHTML = "Horror \n Supernatural \n Comedy";
}

function taktopdestiny() {
    onclick = document.getElementById("anime-name").innerHTML = "Takt op.Destiny"
    onclick = document.getElementById("anime-info").innerHTML = "Destiny (運命), is a Musicart named after Beethoven's Symphony No.5 in C Minor Op.67. She awakens inside Cosette Schneider when she was fatally injured during D2 attack during Symphonica Party. She has made pact with Takt to eliminate D2 and lives insides Cosette Schneider body. Due to her unnatural process of becoming a Musicart, Destiny's appears abnormal compared to other Musicarts.";
    onclick = document.getElementById("show").style.display = "inline";
    onclick = document.getElementById("genre").innerHTML = "Adventure \n Fantasy \n Music";
}

function bonus() {
    onclick = document.getElementById("anime-name").innerHTML = "One Piece"
    onclick = document.getElementById("anime-info").innerHTML = "Gol D. Roger was known as the Pirate King, the strongest and most infamous pirate to have sailed the Grand Line. The capture and death of Roger by the World Government brought a change throughout the world. His last words before his death revealed the location of the greatest treasure in the world, One Piece. It was this revelation that brought about the Grand Age of Pirates, men who dreamed of finding One Piece (which promises an unlimited amount of riches and fame), and quite possibly the most coveted of titles for the person who found it, the title of the Pirate King."
    onclick = document.getElementById("show").style.display = "inline";
    onclick = document.getElementById("genre").innerHTML = "Shounen \n Action \n Adventure \n Comedy \n Mystery";
}